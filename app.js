var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
require('dotenv').config()
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var app = express();
global.appRoot = path.normalize(`${path.resolve(__dirname)}`);
const port = process.env.PORT || 3000;
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger_doc/swagger.json');
//app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use(function(req, res, next) {
  //res.header("Access-Control-Allow-Origin", "http://192.168.1.250:3000");
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
  next();
});


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
const router = express.Router();
app.use('',router);

app.use('/', indexRouter);
app.use('/users', usersRouter);
const usersRoutes = require('./routes/api/v1/users.routes');
usersRoutes(router);
// catch 404 and forward to error handler


const uiOpts = {
  customSiteTitle: "XRB API",
  customCss: ".swagger-ui .topbar { background-color: white; } .swagger-ui .info { margin:0px;} "
};
app.get("/", (req, res) =>
res.send({message: "Welcome to the default API route"})
);

if (process.env.NODE_ENV !== 'production') {
  app.use('/api-doc', function (req, res, next) {
    swaggerDocument.host = req.get('host');
      req.swaggerDoc = swaggerDocument;
      next();
  }, swaggerUi.serve, swaggerUi.setup(swaggerDocument, uiOpts));
}


//Launch listening server on port 8080
app.listen(port,  () => {
console.log(`App listening on port ${port}`);
})

module.exports = app;
