'use strict';

const assetsModel  = require(`${appRoot}/models/users.model`);
const constants  = require(`${appRoot}/config/constant`);
const ObjectId = require('mongodb').ObjectId;

async function update_profile_steptwo(param){
	var error_message ={};
	var check ={};
	
	check.status = true;
	if(param.username=='')
	{
	error_message.username="Please enter username";
	check.status = false;
	}
	if(param.profile_display_name =='')
	{
	error_message.profile_display_name ="Please enter name";
	check.status = false;
	}
	if(param.profile_theme_id=='')
	{
	error_message.profile_theme_id="Please select profile theme id";
	check.status = false;
	}
	check.error_message=error_message;
	return check;
}
async function account_update(param){
	var error_message ={};
	var check ={};	
	check.status = true;	
	if(param.name=='')
	{
		error_message.name="Please enter name";
		check.status = false;
	}
	check.error_message=error_message;
	return check;
}
async function saveSelectUserAssets(param){
	var error_message ={};
	var check ={};
	check.status = true;
	if(param.assets_id=='')
	{
		error_message.assets_id="Please Select Assets id";
		check.status = false;
	}
	check.error_message=error_message;
	return check;
}
async function deleteUserAssets(param){
	var error_message ={};
	var check ={};
	check.status = true;
	if(param.assets_id=='')
	{
		error_message.assets_id="Please Select Assets id";
		check.status = false;
	}
	check.error_message=error_message;
	return check;
}
async function detailsUserAssets(param){
	var error_message ={};
	var check ={};
	check.status = true;
	if(param.assets_id=='')
	{
		error_message.assets_id="Please Select Assets id";
		check.status = false;
	}
	if(param.user_slug=='')
	{
		error_message.user_slug="Please enter user slug";
		check.status = false;
	}
	check.error_message=error_message;
	return check;
}
async function createCollection(param){
	var error_message ={};
	var check ={};
	check.status = true;
	if(param.collection_name=='')
	{
		error_message.collection_name="Please enter collection name";
		check.status = false;
	}
	check.error_message=error_message;
	return check;
}
async function deleteCollection(param){
	var error_message ={};
	var check ={};
	check.status = true;
	if(param.collection_id=='')
	{
		error_message.collection_id="Please select collection id";
		check.status = false;
	}
	check.error_message=error_message;
	return check;
}
async function deleteExhibition(param){
	var error_message ={};
	var check ={};
	check.status = true;
	if(param.exhibition_id=='')
	{
		error_message.exhibition_id="Please select exhibition id";
		check.status = false;
	}
	check.error_message=error_message;
	return check;
}

async function likeCollection(param){
	var error_message ={};
	var check ={};
	check.status = true;
	if(param.collection_slug=='')
	{
		error_message.collection_slug="Please enter collection slug";
		check.status = false;
	}
	if(param.username=='')
	{
		error_message.username="Please enter username";
		check.status = false;
	}
	check.error_message=error_message;	
	return check;
}
async function detailCollection(param){
	var error_message ={};
	var check ={};
	check.status = true;
	if(param.collection_slug=='')
	{
		error_message.collection_slug="Please enter collection slug";
		check.status = false;
	}
	if(param.username=='')
	{
		error_message.username="Please enter username";
		check.status = false;
	}
	check.error_message=error_message;	
	return check;
}
async function editCollection(param){
	var error_message ={};
	var check ={};
	check.status = true;
	if(param.collection_id=='')
	{
		error_message.collection_id="Please enter collection id";
		check.status = false;
	}
	check.error_message=error_message;	
	return check;
}
async function editExhibition(param){
	var error_message ={};
	var check ={};
	check.status = true;
	if(param.exhibition_id=='')
	{
		error_message.exhibition_id="Please enter exhibition id";
		check.status = false;
	}
	check.error_message=error_message;	
	return check;
}
async function slug_url(name){
	const slug = name.replace(/[^A-Z0-9]+/ig, "");
	const uniqid = Math.floor(1000 + Math.random() * 9000);
	const url = slug + uniqid;
	return url;
}
async function likeExhibition(param){
	var error_message ={};
	var check ={};
	check.status = true;
	if(param.exhibition_slug=='')
	{
		error_message.exhibition_slug="Please enter exhibition slug";
		check.status = false;
	}
	if(param.username=='')
	{
		error_message.username="Please enter username";
		check.status = false;
	}
	check.error_message=error_message;	
	return check;
}
async function likeProfile(param){
	var error_message ={};
	var check ={};
	check.status = true;
	if(param.profile_slug=='')
	{
		error_message.profile_slug="Please enter profile slug";
		check.status = false;
	}
	if(param.username=='')
	{
		error_message.username="Please enter username";
		check.status = false;
	}
	check.error_message=error_message;	
	return check;
}
async function likeAllModule(param){
	var error_message ={};
	var check ={};
	check.status = true;
	if(param.section_slug=='')
	{
		error_message.section_slug="Please enter section slug";
		check.status = false;
	}
	if(param.user_slug=='')
	{
		error_message.user_slug="Please enter user_slug";
		check.status = false;
	}
	if(param.section=='')
	{
		error_message.section="Please enter section";
		check.status = false;
	}
	check.error_message=error_message;	
	return check;
}
async function updateFontAllModule(param){
	var error_message ={};
	var check ={};
	check.status = true;
	if(param.font_id=='')
	{
		error_message.font_id="Please enter font id";
		check.status = false;
	}	
	if(param.section=='')
	{
		error_message.section="Please select section";
		check.status = false;
	}
	if(param.section=='collection' || param.section=='exhibition')
	{
		if(param.section_id=='')
		{
			error_message.section_id="Please enter section id";
			check.status = false;
		}
	}
	if(param.update_for=='')
	{
		error_message.update_for="Please select heading / text body";
		check.status = false;
	}
	check.error_message=error_message;	
	return check;
}
async function pageViewTemps(param){
	var error_message ={};
	var check ={};
	check.status = true;
	if(param.asset_id=='')
	{
		error_message.asset_id="Please enter asset id";
		check.status = false;
	}
	if(param.ip_address=='')
	{
		error_message.ip_address="Please enter ip address";
		check.status = false;
	}
	check.error_message=error_message;	
	return check;
}
async function sectionOrdering(param){
	var error_message ={};
	var check ={};
	check.status = true;
	if(param.section=='')
	{
		error_message.section="Please select section";
		check.status = false;
	}
	else 
	{
		if(param.section=='asset' || param.section=='myasset' || param.section=='collection' || param.section=='exhibition') { } else
		{
			error_message.section="Please select valid section";
			check.status = false;
		}
	}	
	if(param.section_id=='')
	{
		error_message.font_id="Please enter section id";
		check.status = false;
	}	
	if(param.order=='')
	{
		error_message.order="Please enter order";
		check.status = false;
	}
	check.error_message=error_message;	
	return check;
}
module.exports ={
	update_profile_steptwo,
	account_update,
	saveSelectUserAssets,
	deleteUserAssets,
	detailsUserAssets,
	likeCollection,
	createCollection,
	deleteCollection,
	deleteExhibition,
	detailCollection,
	slug_url,
	likeExhibition,
	likeProfile,
	likeAllModule,
	updateFontAllModule,
	pageViewTemps,
	sectionOrdering,
	editCollection,
	editExhibition
}
