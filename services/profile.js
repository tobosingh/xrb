'use strict';
const constants = require(`${appRoot}/config/constant`);
const usersModel  = require(`${appRoot}/models/users.model`);
const profileLikeModel  = require(`${appRoot}/models/user_profile_like.model`);
const ObjectId = require('mongodb').ObjectId;
const validation = require(`${appRoot}/services/validation`);
async function likeUserProfile(req){
	console.log(req.query);
	if(!req.query.section_slug){
		return false
	}
	const profile_slug=req.query.section_slug
	let records = await usersModel.findOne({"user_slug" : req.query.user_slug});
	if(records){
		const wallet_id =records.wallet_id;
		const user_id =records._id;
		let profile_records = await usersModel.findOne({"user_slug":profile_slug});
		if(profile_records)
		{
			const profile_id =profile_records._id;
			let like_count=profile_records.total_profile_like;
			like_count=parseInt(like_count);
			let like_data= await profileLikeModel.findOne({user_id,profile_id});
			let message=''
			let like_status=''
			let updated_at= new Date()
			if(like_data)
			{
				if(like_data.status==2)
				{
					if(like_count>0) {  like_count++; } else { like_count=1; }
					message="Profile Liked";
				    like_status=1;
					await profileLikeModel.updateOne({user_id,profile_id},{"status":1,"updated_at":updated_at});
					let data=await usersModel.updateOne({_id:ObjectId(profile_id)},{$inc: { total_profile_like:1}});
				}
				else
				{
					if(like_count>0) {  like_count--; } else { like_count=1; }
					message="Profile disliked";
				    like_status=2;
					await profileLikeModel.updateOne({user_id,profile_id},{"status":2,"updated_at":updated_at});
					let data=await usersModel.updateOne({_id:ObjectId(profile_id), total_profile_like: { $gt: 0 }},{$inc: { total_profile_like:-1}});
				}
			}
			else
			{
				const newprofileLike = new profileLikeModel({user_id, profile_id,status:1});
				const newData = await newprofileLike.save();
				if(like_count>0) {  like_count++; } else { like_count=1; }
				message="Profile Liked";
				like_status=1;
				let data=await usersModel.updateOne({_id:ObjectId(profile_id)},{$inc: { total_profile_like:1}});
			}
			return {like_status:like_status,like_count:like_count,message:message}
		}
		else {
			return false
		}
	}else{
		return false
	}
	
}


module.exports ={
	likeUserProfile
}
