//var config = require('../../../config/index');
var constant = require(`${appRoot}/config/constant`);
const usersModel  = require(`${appRoot}/models/users.model`);
const utils = require(`${appRoot}/middleware/utils`)
var crypto = require('crypto');
var request = require('request');
var async = require('async');
var jwt = require('jsonwebtoken');
var moment = require('moment');
const ObjectId = require('mongodb').ObjectId;
const HttpStatus = require('http-status');
const validation = require(`${appRoot}/services/validation`);
const commanHelper = require('../../../helpers/common.helper');

async function login (req, res){
	console.log(req.body);
	let result = {};
	let records = await usersModel.findOne({ wallet_id : req.body.wallet_id,  deleted_at : 0},{_id: 1,wallet_id:1,user_type:1,username:1,user_slug:1,profile:1});
	if(records){
		let record = {};
			record._id=records._id;
			record.wallet_id=records.wallet_id;
			record.user_type=records.user_type;
			record.username=records.username;
			record.user_slug=records.user_slug;
			record.profile=records.profile;
			//record.user_status='User Active';
			//const updatedata = await commanHelper.updateTotalValue(record.username);
			const token = await utils.generate_token_for_user(record);
			return res.send({ return_code : HttpStatus.OK, return_status : 0, data : record, token : token });
		
	}
	else{
		try{
			let newUser = new usersModel({ wallet_id : req.body.wallet_id,status: 1,email:req.body.email,referal_code:req.body.code});
			const userData = await newUser.save();
			console.log(userData);
			if(userData){
				const token = await utils.generate_token_for_user(userData);
				//return res.send({ return_code : HttpStatus.OK, return_status : 0, message : "User has been created successfully", data : userData, token : token });
				let recorduserData = {};
				recorduserData._id=userData._id;
				recorduserData.wallet_id=userData.wallet_id;
				recorduserData.user_type=userData.user_type;
				recorduserData.username=userData.username;
				recorduserData.user_slug=userData.user_slug;
				recorduserData.profile=userData.profile;
				return res.send({ return_code : HttpStatus.OK, return_status : 0, message : "User has been created successfully", data : recorduserData, token : token });
			}
		}catch(err){
			var error_message ={};
			if(err.errors && err.errors.wallet_id)
			{
			error_message.wallet_id="Please enter wallet id";
			}
			return res.send({ return_code : HttpStatus.FORBIDDEN, return_status : 1, message :error_message, data : {}});
		}
	}
}

async function update_profile_steptwo(req, res){
	try{
			console.log(req.body);
			const slug_url = await validation.slug_url(req.body.profile_display_name);
			console.log(slug_url);
			const validation_data = await validation.update_profile_steptwo(req.body);
			if (validation_data.status==false) {
				return res.send({ return_code : HttpStatus.FORBIDDEN, return_status : 1, message :validation_data.error_message, data : {}});
			}
			let recordPTM = await profileThemeMasterModel.findOne({ _id : new ObjectId(req.body.profile_theme_id)},{profile_master_asset_url: 1,default_setting:1,_id:1});
			
			var profile_master_asset_url='';
			var default_setting ='';
			if(recordPTM) {
				profile_master_asset_url=recordPTM.profile_master_asset_url;
				default_setting = recordPTM.default_setting
			}
			//console.log(default_setting.theme);
			let record = await usersModel.findOne({ wallet_id : req.body.mask_id,  deleted_at : 0, status : 0});
			if(record && recordPTM){
				let updated_data = {};
				updated_data.register_step = 4;
				let profile_data = record.profile;
				if (req.body.username) {
					updated_data.username=req.body.username;
					updated_data.user_slug = req.body.username;
				}
				if (req.body.profile_display_name) {
					profile_data.profile_display_name = req.body.profile_display_name;
				}
				profile_data.profile_avatar = default_setting.profile.profile_avatar;

				updated_data.profile = profile_data;
				updated_data.user_type = 'creator';
				updated_data.status = 1;
				updated_data.theme = default_setting.theme;
				updated_data.theme.home_theme_id = req.body.profile_theme_id;
				updated_data.theme.home_theme_asset_url = profile_master_asset_url;

				updated_data.theme.collection_thumb_crop_class = "coverImg";
				updated_data.theme.exhibition_thumb_crop_class = "coverImg";
				updated_data.theme.myasset_thumb_crop_class = "coverImg";

				//console.log(">>>>>>>>"+recordPTM._id);
				//updated_data.home_theme_id = recordPTM._id;
				// theme static data section
				/*var hero_banner_config = {
					banner_img_url : '',
					banner_color : '',
					banner_color_mode:''
				};*/
				/*var home_background_detail = {
					background_img_url : '',
					background_color : '',
					background_color_mode:''
				};*/
				/*var typography = {
					font_family_heading : 'Arial',
					font_family_text_body :'Arial'
				};
				*/
				/*var title_text_color =  {
					"color": "#ffff00",
					"mode": "gradient"
				};*/
				/*var body_text_color =  {
					"color": "#ffff00",
					"mode": "solid"
				};*/
				/*var button_background_color =  {
					"color": "#ffff00",
					"mode": "solid"
				};*/
				/*var button_border_color = {
					"color": "#ffff00",
					"mode": "gradient"
				};*/
				/*var slide_button_color = {
					"color": "#ffff00",
					"mode": "solid"
				};*/
				/*var color_code = {
					title_text_color : title_text_color,
					body_text_color : body_text_color,
					button_background_color : button_background_color,
					button_border_color : button_border_color,
					slide_button_color : slide_button_color
				};*/
				/*var home_theme_config = {
					hero_banner_config:hero_banner_config,
					home_background_detail:home_background_detail,
					//font_color_detail:color_code,
					font_detail : typography
				};*/
				
				/*var theme_data = {
					home_theme_id :req.body.profile_theme_id,
					home_theme_asset_url:profile_master_asset_url,
					home_theme_config : home_theme_config,
					font_color_detail:color_code,
					//font_detail : typography
					theme_updated_at : Date.now,
					theme_updated_by : ''
				};*/


				//updated_data.theme = theme_data;
				// theme static data section
				await usersModel.updateOne({ wallet_id : req.body.mask_id }, updated_data);
				let data = await usersModel.findOne({ wallet_id : req.body.mask_id},{_id: 1,wallet_id:1,user_type:1});
				return res.send({ return_code : HttpStatus.OK, return_status : 0, message : "Userdata has been updated successfully ", data : data });
			}else{
				return res.send({ return_code : HttpStatus.FORBIDDEN, return_status : 1, message :"User not register", data : []});
			}
		}catch(err){
			console.log(err);
			return res.send({ return_code : HttpStatus.FORBIDDEN, return_status : 1, message :"Something went wrong", data : []});
		}
}

async function checkusername(req, res){
	let record = await usersModel.findOne({ username : req.body.username, wallet_id : {$ne : req.body.mask_id}});
	if(record){
		return res.send({ return_code : HttpStatus.FORBIDDEN, return_status : 1, return_message : "Username is not available",user_exist:"Y" ,request_id :'' });
	}
	else{
		return res.send({ return_code : HttpStatus.OK, return_status : 0, return_message : "Username available",user_exist:"N", request_id :''});
	}
}

module.exports = {
	login,
	update_profile_steptwo,
	checkusername
}
