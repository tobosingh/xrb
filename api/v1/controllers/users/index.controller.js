const commanHelper = require('../../helpers/common.helper');
const constant = require(`${appRoot}/config/constant`);
const usersModel  = require(`${appRoot}/models/users.model`);
const utils = require(`${appRoot}/middleware/utils`);
var crypto = require('crypto');
var request = require('request');
var async = require('async');
var jwt = require('jsonwebtoken');
var moment = require('moment');
const ObjectId = require('mongodb').ObjectId;
const HttpStatus = require('http-status');
const validation = require(`${appRoot}/services/validation`);
const fetch = require("node-fetch");

// const cron = require('cron');

// const job = new cron.CronJob({
//   cronTime: '*/30 * * * *',
//   onTick: function() {
//     fetchAllDataUser();
//     console.log('Cron jub runing fetch data...');
//   },
//   start: true,
//   timeZone: 'Asia/Ho_Chi_Minh'
// });



// job.start();


// async function fetchAllDataUser(){
// 	try{
// 		let records = await usersModel.find({fetch_nft_status : true,user_type: 'visitor'});
// 		if(records && records.length > 0){
// 			for(let i = 0 ; i < records.length ; i++){
// 				if(records[i].wallet_id && validateInputAddresses(records[i].wallet_id) ){
// 					let param = {};
// 					param.query = {
// 						page : 1,
// 						limit: 100
// 					}
// 					param.wallet_id = records[i].wallet_id;
// 					let assetsData = await assetsService.fetchDataNftFull(param);
// 					await assetsService.updateAssets(assetsData.data,records[i].wallet_id);
// 				}
// 			}
// 		}else{
// 			console.log("no account need fetch data");
// 		}

// 	}catch(err){
// 		console.log(err);
// 	}
// }

function validateInputAddresses(address) {
	return (/^(0x){1}[0-9a-fA-F]{40}$/i.test(address));
}

async function generate_token (req, res){
	try {
		const generate_log_id = await commanHelper.requestIdGenerate('token');
		var result = {};
		if(constant.KEY == req.body.key && constant.SECRET_KEY==req.body.secret_key){
			const payload = { key : req.body.key, secret_key : req.body.secret_key, exp : Math.floor(Date.now() / 1000) + (60 * 60 * 24)};
			const secret = constant.JWT_SECRET;
			const token = jwt.sign(payload, secret);
			let status = 0;
			result.token = token;
			result.status = status;
			//res.status(status).send(result);
			return res.send({ return_code : HttpStatus.OK, data : result, return_status : 0, message : 'Token generated successfully',request_id:generate_log_id})
		}else{
			return res.send({ return_code : HttpStatus.FORBIDDEN, return_status : 1, message : "Invalid key or secret key, Please check.", data : {} ,request_id:generate_log_id});
		}
		
	}catch(err){
		return res.send({ return_code : HttpStatus.FORBIDDEN, return_status : 1, message : "Something went wrong", data : {} ,request_id:generate_log_id});
	}
}

async function updatedata (req, res){
	try {
		await usersModel.updateMany({ }, {register_step:1});
		return res.send({ return_code : HttpStatus.OK, return_status : 0 ,message : 'Data Update successfully'})
	}catch(err){
		return res.send({ return_code : HttpStatus.FORBIDDEN, return_status : 1, message : "Something went wrong", data : {} });
	}
}
async function bitkubNext(req, res){
	 
    var request_code = req.query.code;
    const BITKUB_ACCOUNT_URL = 'https://accounts.bitkubnext.com'
    console.log(request_code);
// exchange the oauth2 response code to access token and refresh token
const exchangeAuthorizationCode = async (clientId, redirectURI, code) => {
    //try {
        const url = BITKUB_ACCOUNT_URL + '/oauth2/access_token'
       
        const request = await fetch(url, {
            method: 'POST',
            headers: {
                Authorization: `Basic ${Buffer.from(`${clientId}:`).toString('base64')}`,
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            },
            body: new URLSearchParams({
                grant_type: 'authorization_code',
                client_id: clientId,
                redirect_uri: redirectURI,
                code:code,
            }),
        });
        const res_data = await request.text();

        return res.send(res_data+'||'+request_code);
}


exchangeAuthorizationCode('62209adcc4e80866ea6aef32','https://meta-app01.herokuapp.com/api/oauth/bitkubnext/callback',request_code);


}

async function deactivateUser(req, res){
	let usersModels=await usersModel.findOne({wallet_id:req.body.mask_id});
	if (usersModels) {
		await usersModel.updateOne({wallet_id:req.body.mask_id}, {"status":'2'});
				return res.send({ return_code : HttpStatus.OK, return_status : 0, message : "User Deactivated successfully", data : '' });
		}else{
			return res.send({ return_code : HttpStatus.FORBIDDEN, return_status : 1, message :validation_data.error_message});
	}
}
async function activateUser(req, res){
	let usersModels=await usersModel.findOne({wallet_id:req.body.mask_id});
	if (usersModels) {
		await usersModel.updateOne({wallet_id:req.body.mask_id}, {"status":'1'});
				return res.send({ return_code : HttpStatus.OK, return_status : 0, message : "User Activated successfully", data : '' });
		}else{
			return res.send({ return_code : HttpStatus.FORBIDDEN, return_status : 1, message :validation_data.error_message});
	}
}
module.exports = {
	generate_token,
	updatedata,
	bitkubNext,
	deactivateUser,
	activateUser,
	validateInputAddresses
}
