require('dotenv').config()
const fs = require('fs');
const path = require('path');
const { S3_FOLDERS } = require('../../../config/constant');

var config = require('../../../config/index');
var constant = require('../../../config/constant');
var request = require("request");
var async = require('async');
var moment = require('moment');
const ObjectId = require('mongodb').ObjectId;

const usersModel  = require(`${appRoot}/models/users.model`);
const apiLogsModel = require(`${appRoot}/models/api_logs.model`);

async function send_email(mailOptions){
		let transporter = nodeMailer.createTransport({
          host: 'smtp.gmail.com',
          port: 587,
          secure: false,
          auth: {
              user: 'chalat.smoothgraph@gmail.com',
              pass: '@SuperRich2018!'
          }
      	});
		transporter.sendMail(mailOptions, function(error, info){
			console.log(error);
		  	if(error){
				return 2;
			}else{
				return 1;
			}
	   });
	}

	async function uploadFile(files,fieldName,imgpath){
		if(files && Object.keys(files).length != 0 &&  files[fieldName] && files[fieldName].name)
		{
		  const imgname = Date.now()+'.'+(files[fieldName].name).split('.').pop();
		  try {
				await files[fieldName].mv(imgpath + imgname);
				if (process.env.NODE_ENV === 'production') {
					return `${constant.API_URL}/${imgpath + imgname}`.replace('\/public', '');
				}
        const url = await uploadS3File(imgpath+imgname, files[fieldName].mimetype);
			  return url;
		  } catch (e) {
			  console.log(e);
			return "";
		  }
		}else{
			console.log("===imagenot uploaded successfully===");
		  return "";
		}
	}

const hasText = (str) => {
  const testStr = `${str || ""}`;
  return !!str && testStr.length > 0;
};

async function uploadS3File (filePath, contentType = "image/png", folder = S3_FOLDERS.IMAGES, isReadStream = true, fileContent = null) {
  const fileStream = isReadStream ? fs.createReadStream(filePath) : fileContent;
  const params = {
    Bucket: S3_INFO.BUCKET,
    Key: `${folder}/${path.basename(filePath)}`,
    Body: fileStream,
		ACL: 'public-read'
  };
  if (hasText(contentType)) {
    params.ContentType = contentType;
  }
  
  const data = await s3.upload(params).promise();
  return data.Location;
};

	async function requestIdGenerate(req){
		const gen = Math.floor(100000 + Math.random() * 900000);
		return req+'-'+gen;
	}

async function insertLog(response_obj, section, data_obj) {
	try {
		var obj = {
			request_id: data_obj.request_id,
			reference_id: "",
			section: section,
			action: data_obj.action,
			status: data_obj.status,
			response: response_obj,
			json_data: {},
		};
		await apiLogsModel.create(obj);
	} catch (err) {
		console.log(err);
	}
}

async function getFormattedId(min = 100, max = 200) {
	const d_t = new Date();

	let year = d_t.getFullYear();
	let month = ("0" + (d_t.getMonth() + 1)).slice(-2);
	let day = ("0" + d_t.getDate()).slice(-2);
	let hour = d_t.getHours();
	let minute = d_t.getMinutes();
	let seconds = d_t.getSeconds();
	let number = Math.floor(Math.random() * (max - min) + min);
	return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + seconds + "_" + number;
}

module.exports = {
	uploadFile,
	send_email,
	requestIdGenerate,
	insertLog,
	getFormattedId,
	uploadS3File
}
