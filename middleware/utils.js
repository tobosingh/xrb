const jwt = require('jsonwebtoken');
const constant = require('../config/constant');
module.exports = {
  validateToken: (req, res, next) => {
	  
    const authorizationHeaader = req.headers.authorization;
    let result='';
    if (authorizationHeaader) {	
      const token = authorizationHeaader;
      try {
			result = jwt.verify(token, constant.JWT_SECRET);
			console.log(result);
			if(result.key == constant.KEY && result.secret_key ==constant.SECRET_KEY){
				next();
			}else{
			  result = { 
				error: `Authentication error. Invalid token.`,
				status: 401 
			  };
			  res.status(401).send(result);
			}
      } catch (err) {
		  console.log("test4");
        //throw new Error(err);
        result = { 
			error: `Authentication error. Invalid token.`,
			status: 401 
		  };
		  res.status(401).send(result);
      }
    } else {
		console.log("error data");
      result = { 
        error: `Authentication error. Token required.`,
        status: 401 
      };
      res.status(401).send(result);
    }
  },

  validateTokenUser: (req, res, next) => {	
  const authorizationHeaader = req.headers.authorization;
  let result='';
  if (authorizationHeaader) {	
	const token = authorizationHeaader;
	try {
		  result = jwt.verify(token, constant.JWT_SECRET);		 	  
		  console.log(result.data.wallet_id);
		  if(result.data && result.data.wallet_id && result.key == constant.KEY && result.secret_key == constant.SECRET_KEY){
			req.body.mask_id = result.data.wallet_id;
			  next();
		  }else{
			result = { 
			  error: `Authentication error. Invalid token.`,
			  status: 401 
			};
			res.status(401).send(result);
		  }
	} catch (err) {
		console.log("test4");
	  //throw new Error(err);
	  result = { 
		  error: `Authentication error. Invalid token.`,
		  status: 401 
		};
		res.status(401).send(result);
	}
  } else {
	  console.log("error data");
	result = { 
	  error: `Authentication error. Token required.`,
	  status: 401 
	};
	res.status(401).send(result);
  }
},
  
 generate_token : (data) => {	 
		const payload = { data : data, expiresIn: '730d' };
		const secret = constant.JWT_SECRET;	
		const token = jwt.sign(payload, secret);
		return token;
  },

  generate_token_for_user : (data) => {	
	 const record = {
		wallet_id : data.wallet_id
	};
	console.log(record);
	const payload = { data : record, key : constant.KEY, secret_key : constant.SECRET_KEY, expiresIn: '730d' };
	const secret = constant.JWT_SECRET;	
	const token = jwt.sign(payload, secret);
	return token;
}
  
};


