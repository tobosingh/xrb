var indexController = require('../../../api/v1/controllers/users/index.controller');
var setupController = require('../../../api/v1/controllers/users/setup/index.controller');
var validateToken = require(`${appRoot}/middleware/utils`).validateToken;
let validateTokenUser = require(`${appRoot}/middleware/utils`).validateTokenUser;
var cors = require('cors');
module.exports = function(router) {
	router.post('/api/v1/users/generate_token', indexController.generate_token);
	router.all('/api/v1/users/setup/login', setupController.login);
}
