var mongoose = require('mongoose');
var DataSchema = new mongoose.Schema({
  name  :
    {
      type : String,
      required: true
    },
  address :
    {
      type : String,
      required: true
    },
  image :
    {
      type : String,
      default : ''
    },
  type :
    {
      type : String,
      default : ''
    },
  order :
    {
      type : Number
    },
  disable_transfer :
    {
      type : String,
      default : ''
    },
  available_set_profile :
    {
      type : String,
      default : ''
    }
},{
  versionKey: false
})

var DataExports = mongoose.model('nft_tokens', DataSchema)
module.exports = DataExports;
