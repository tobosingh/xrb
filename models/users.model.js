var mongoose = require('mongoose');
var UsersSchema = new mongoose.Schema({
	wallet_id : 
	{
		type : String,
		required:true
	},
	user_type : 
	{
		type : String,
		default : 'visitor'
	},
	username : 
	{
		type : String,
		default:null
	},	
	user_slug : 
	{
		type : String,
		default : ''
	},
	profile : 
	{
        type : Object,
        default : {}
	}, 
	theme : 
	{
        type : JSON,
        default : {}
	}, 
	total_assets	: 
	{
        type : Number,
        default : 0
	}, 		
	total_asset_like	: 
	{
        type : Number,
        default : 0
	},
	total_collection	: 
	{
        type : Number,
        default : 0
	}, 	
	total_exhibition	: 
	{
        type : Number,
        default : 0
    }, 	
    total_profile_like	: 
	{
        type : Number,
        default : 0
	},	 	
	total_collection_like	: 
	{
        type : Number,
        default : 0
	}, 	
	total_exhibition_like	: 
	{
        type : Number,
        default : 0
    }, 		
	status : 
	{
		type: Number,
		default : 1
	},
	fetch_nft_status: 
	{
		type: Boolean,
		default : false
	},

	register_step : 
	{
		type: Number,
		default : 1
	},
	deleted_at	: 
	{ 
		type : Number, 
		default : 0
	},	
	created_at	: 
	{
        type : Date,
        default : Date.now
    },
    updated_at : 
    {
        type: Date,
        default: Date.now
    }  
},{
    versionKey: false
})

var Orders = mongoose.model('users', UsersSchema)
module.exports = Orders;
