var mongoose = require('mongoose');
var ApiLogSchema = new mongoose.Schema({
	request_id : 
	{
		type : String,
		required:true
	},
	reference_id : 
	{
		type : String
	},
	section : 
	{
		type : String
	},	
	wallet_id : 
	{
		type : String		
	},
	action : 
	{
        type : String        
	}, 
	status : 
	{
        type: Number,     
	}, 
	response	: 
	{
        type: Object,
		default:{}
	}, 		
	json_data	: 
	{
        type : Object,
		default:{}
	},			
	created_at	: 
	{
        type : Date,
        default : Date.now
    },
    updated_at : 
    {
        type: Date,
        default: Date.now
    }  
},{
    versionKey: false,
	minimize: false
})

var ApiLogs = mongoose.model('api_log', ApiLogSchema)
module.exports = ApiLogs;
