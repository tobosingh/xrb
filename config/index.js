module.exports = {
	connection : require('./db.js'),
	constant   : require('./constant.js'),
}
//helper call
var helpers = {};
const path = require("path");
const fs = require('fs');
//const modelsPath = path.resolve(__dirname, '../api/oms/v1/helpers');
const modelPath = path.resolve(__dirname, '../api/v1/helpers');

fs.readdirSync(modelPath).forEach(file => {
 var m_name = file.split(".")[0]; 	
 	helpers[m_name] = require(modelPath + '/' + file); 
});
module.exports.helpers = helpers;
